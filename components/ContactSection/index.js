import React, { useState } from "react";
import ContactForm from "../ContactFrom";
import Slider from "react-slick";
import axios from "axios";

const ContactSection = () => {
  const [state, setState] = React.useState({
    firstname: "",
    lastname: "",
    mail: "",
    message: "",
    phone: "",
    botfield: "",
  });

  const [errorFirstName, setErrorFirstName] = useState("");
  const [errorLastName, setErrorLastName] = useState("");
  const [errorContact, setErrorContact] = useState("");

  const [loadingSend, setLoadingSend] = useState(false);
  const [messageSent, setMessageSent] = useState(false);

  function handleChange(e) {
    setMessageSent(false);
    if (e.target.name == "firstname") {
      setErrorFirstName("");
    }
    if (e.target.name == "lastname") {
      setErrorLastName("");
    }
    if (e.target.name == "mail" || e.target.name == "phone") {
      setErrorContact("");
    }
    if (e.target.files) {
      setState({ ...state, [e.target.name]: e.target.files[0] });
    } else {
      setState({ ...state, [e.target.name]: e.target.value });
    }
  }

  async function handleSubmit(e) {
    e.preventDefault();
    if (
      state.botfield == "" &&
      state.firstname != "" &&
      state.lastname != "" &&
      (state.mail != "" || state.phone != "")
    ) {
      setLoadingSend(true);
      let formData = new FormData();
      for (let [key, value] of Object.entries(state)) {
        formData.append(key, value);
      }
      await axios
        .post(
          "https://formeezy.com/api/v1/forms/6228a37b75c8f10009fb3321/submissions",
          formData
        )
        .then(({ data }) => {
          const { redirect } = data;
          setMessageSent(true);
          setLoadingSend(false);
          var elements = document.getElementsByTagName("input");
          for (var ii = 0; ii < elements.length; ii++) {
            if (elements[ii].type == "text") {
              elements[ii].value = "";
            }
          }
        })
        .catch((e) => {});
    } else {
      if (state.firstname == "") {
        setErrorFirstName("Merci d'indiquer votre prénom.");
      }
      if (state.lastname == "") {
        setErrorLastName("Merci d'indiquer votre nom.");
      }
      if (state.mail == "" && state.phone == "") {
        setErrorContact(
          "Merci d'indiquer soit votre e-mail, soit votre numéro de téléphone afin de vous contacter."
        );
      }
    }
  }

  return (
    <section id="contact" className="tp-contact-pg-section section-padding">
      <div className="container">
        <div className="row">
          <div className="col col-lg-10 offset-lg-1">
            <div className="office-info">
              <div className="row">
                <div
                  className="col col-xl-6 col-lg-6 col-md-6 col-12"
                  data-aos="fade-up"
                >
                  <a href="mailto:constantinkrieg@gmail.com">
                    <div className="office-info-item">
                      <div className="office-info-icon">
                        <div className="info-icon">
                          <i className="fi flaticon-mail"></i>
                        </div>
                      </div>
                      <div className="office-info-text">
                        <h2>E-mail</h2>
                        <p>constantinkrieg@gmail.com</p>
                      </div>
                    </div>
                  </a>
                </div>
                <div
                  className="col col-xl-6 col-lg-6 col-md-6 col-12"
                  data-aos="fade-up"
                >
                  <a href="tel:+33543460886">
                    <div className="office-info-item">
                      <div className="office-info-icon">
                        <div className="info-icon">
                          <i className="fi flaticon-telephone"></i>
                        </div>
                      </div>
                      <div className="office-info-text">
                        <h2>Téléphone</h2>
                        <p>06 43 46 08 86</p>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            </div>
            <div
              className="section-title section-title2 text-center"
              data-aos="fade-up"
            >
              <span>Contact</span>
              <h2 class="no-uppercase">N'hésitez pas à me contacter !</h2>
            </div>
            <div className="tp-contact-form-area" data-aos="fade">
              <form onSubmit={handleSubmit} className="form">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-12">
                    <formGroup className="form-field">
                      <input
                        onChange={handleChange}
                        type="text"
                        name="firstname"
                        placeholder="Prénom"
                      />
                      <p>{errorFirstName}</p>
                    </formGroup>
                  </div>
                  <div className="col-lg-6 col-md-6 col-12">
                    <formGroup className="form-field">
                      <input
                        onChange={handleChange}
                        type="text"
                        name="lastname"
                        placeholder="Nom"
                      />
                      <p>{errorLastName}</p>
                    </formGroup>
                  </div>
                  <div className="col-lg-6 col-md-6 col-12">
                    <formGroup className="form-field">
                      <input
                        onChange={handleChange}
                        type="email"
                        name="mail"
                        placeholder="E-mail"
                      />
                    </formGroup>
                  </div>
                  <div className="col-lg-6 col-md-6 col-12">
                    <formGroup className="form-field">
                      <input
                        onChange={handleChange}
                        type="phone"
                        name="phone"
                        placeholder="Téléphone"
                      />
                    </formGroup>
                  </div>
                  <div className="col-lg-12">
                    <formGroup className="form-field">
                      <p>{errorContact}</p>
                    </formGroup>
                  </div>
                  <div className="col-lg-12">
                    <formGroup className="form-field">
                      <textarea
                        onChange={handleChange}
                        name="message"
                        placeholder="Message"
                      ></textarea>
                    </formGroup>
                  </div>
                  <div className="col-lg-12">
                    <input
                      onChange={handleChange}
                      type="text"
                      name="bot-field"
                      style={{ display: "none" }}
                    />
                  </div>
                  <div className="col-lg-12">
                    {messageSent && (
                      <div className="confirm-message" data-aos="fade">
                        <i class="fa fa-check" aria-hidden="true"></i>
                        <p>Le message a été envoyé avec succès.</p>
                      </div>
                    )}
                  </div>
                  <div className="col-lg-12">
                    {!messageSent && (
                      <div className="form-submit">
                        {!loadingSend && (
                          <button
                            type="submit"
                            className="template-btn btn-rainbow mt-3"
                            data-aos="fade"
                          >
                            Envoyer
                          </button>
                        )}
                        {loadingSend && (
                          <p className="my-3" data-aos="fade">
                            Envoi en cours...
                          </p>
                        )}
                      </div>
                    )}
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      {/*
            <section className="tp-contact-map-section">
                <div className="tp-contact-map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d193595.9147703055!2d-74.11976314309273!3d40.69740344223377!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sbd!4v1547528325671"></iframe>
                </div>
            </section>
            */}
    </section>
  );
};

export default ContactSection;
