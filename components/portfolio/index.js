import React, { useState } from "react";
import Link from "next/link";
import PortfolioSingle from "../portfolioSingle";
import Image from "next/image";

const Portfolio = () => {
  const [open, setOpen] = React.useState(false);

  function handleClose() {
    setOpen(false);
  }

  const [state, setState] = useState({});

  const handleClickOpen = (item) => {
    setOpen(true);
    setState(item);
  };

  const portfolio = [
    {
      Id: "1",
      heading: "Parcel'o",
      subHeading: "L'app pour aider les clients et gérants de points relais.",
      type: "Application & Installation",
      job: "UX/UI Lead Designer & Développeur",
      date: "2021",
      videosId: "LUSa3yRTB9A",
      cover: "/static/images/portfolio/parcelo/cover.webp",
      pImg1: "/static/images/portfolio/parcelo/img-1.webp",
      pImg2: "/static/images/portfolio/parcelo/img-2.webp",
      pImg3: "/static/images/portfolio/parcelo/img-3.webp",
      vedio: "",
      text1:
        "Parcel'o est une application conçue suite à la demande d'un point relais afin de dynamiser et de moderniser sa productivité. Parcel'o a par la suite évolué afin de pouvoir viser et être utilisée par un nombre plus conséquent de points relais. La gestion des colis peut être parfois compliquée. En tant que commerçant, beaucoup de temps de travail utile peut être perdu à s’occuper d’un colis en même temps de son activité principale. Suite à une analyse au sein de ce point relais, il a été remarqué que demander les informations nécessaires pour trouver le colis prenait environ 2 à 4 minutes. Parcel’o a pour mission de réduire ce temps d’attente, et donc d’optimiser le temps de travail. Pour les clients, l’application permet de réunir l’ensemble de leurs colis sur un seul outil de gestion et de pouvoir afficher le suivi et les informations des colis en temps réel.",
      text2:
        "Les clients ont la possibilité d’ajouter leurs différents colis et accéder à leur suivi en temps réel, grâce à une connexion au suivi des différents transporteurs (Mondial Relay, Relais Colis, UPS,...). Une fois qu'un colis est livré, ils recevront une notification sur leur téléphone. Si le point relais est équipé d'une borne Parcel'o, il suffira de scanner sur place le code généré afin d'envoyer toutes les informations nécessaires pour que le colis soit délivré efficacement. Dans le cas contraire, ils disposent quand même des informations sous format textuelle à transmettre à son point relais.",
      text3:
        "Les points relais équipés d’une borne Parcel’o, recevront une notification sur leur application lorsqu’un client scannera son code pour venir retirer son colis. Les commerçants disposent également d'autres fonctionnalités comme des analyses statistiques afin de connaître par exemple : une estimation de l'argent gagné cette semaine grâce au point relais, son pic de trafic, ou encore le temps moyen pour la livraison d'un colis. Prendre en compte ces données, permettront de booster les performances de la boutique.",
      quote:
        "Étant un habitué pour récupérer mes colis en points relais, ce projet a été très enrichissant, car j'ai pu découvrir l'autre dimension, non pas celle des clients à laquelle j'étais habitué, mais la complexité actuelle de gestion d'un point relais. Une fois sur place, j'ai pu avoir des feedback des utilisateurs (autant client comme du gérant) et améliorer au fur et à mesure l'expérience proposée !",
    },
    {
      Id: "2",
      heading: "Bulby",
      subHeading:
        "Le compagnon connecté qui apprend les bases du jardinage à votre enfant.",
      type: "Application & IoT",
      job: "UX/UI Lead Designer",
      date: "2016",
      videosId: "r_hYR53r61M",
      cover: "/static/images/portfolio/bulby/cover.webp",
      pImg1: "/static/images/portfolio/bulby/img-1.webp",
      pImg2: "/static/images/portfolio/bulby/img-2.webp",
      pImg3: "/static/images/portfolio/bulby/img-3.webp",
      vedio: "",
      text1:
        "Bulby est un projet issu d'un challenge organisé par Leroy Merlin (Groupe Adéo) où de nombreux étudiants de divers secteurs (HEC, Telecom, 42, e-artsup,...) devaient proposer une solution aux différents enjeux que proposait le client. Notre défi avec Bulby a été de concevoir un objet connecté qui invite les enfants de 4-7 ans à s’initier au jardinage, tout en favorisant l’échange parent-enfant au quotidien.",
      text2:
        "Bulby est un ensemble d'objets et une application : il est composé d'un capteur que l'on plante dans un jardin où les informations d'humidité, de température de luminosité, etc. sont directement retranscrites sur deux interfaces complémentaires et distinctes : une veilleuse en forme de hérisson et une application.",
      text3:
        "L'usage de l'application servira de plateforme d'échange et d'activité entre l'enfant et le parent où ils pourront voir accès à différentes fonctionnalités comme voir les besoins de la plante en temps réel, afficher des histoires interactives pour l'enfant en fonction des actions qu'il a réalisé aujourd'hui en s'occupant de sa plante. Reprenant la forme d’animaux du jardin (ici le hérisson), la veilleuse vit et s’anime au rythme de la plante, notifiant l’enfant d’une action à prendre. Une lumière douce avec un rythme lent indiquera à l'enfant que sa plante est en bonne santé, mais une lumière terne avec un rythme plus élevé lui signaleront que sa plante a besoin de son aide. L'interaction avec la veilleuse se fait par le toucher. En une caresse, elle s’éveille et s’endort.",
    },
    {
      Id: "3",
      heading: "Sogeco",
      subHeading: "Gérer votre budget étudiant n'a jamais été aussi simple.",
      type: "Application",
      job: "UX/UI Designer",
      date: "2018",
      videosId: "LUSa3yRTB9A",
      cover: "/static/images/portfolio/sogeco/cover.webp",
      pImg1: "/static/images/portfolio/sogeco/img-1.webp",
      pImg2: "/static/images/portfolio/sogeco/img-2.webp",
      pImg3: "/static/images/portfolio/sogeco/img-3.webp",
      vedio: "",
      text1:
        "Sogeco est un outil de gestion du compte bancaire permettant de maîtriser son budget mensuel à travers une expérience utilisateur unique.",
      text2:
        "Parti du constat que beaucoup d’étudiant a du mal à commencer en s’auto-gérant, Sogeco permettra de l'accompagner jusqu'à ce que cela devienne un automatisme. L'application permet de créer un budget généré avec un suivi automatique des dépenses de l'utilisateur sur une base mensuel. Par exemple, s'il ne sait pas combien allouer à son budget Loisirs, l'application analysera les mois précédents, fera une moyenne et proposera un budget raisonnable et personnalisé.",
      text3: "",
      quote:
        "Je venais de finir mes études lorsque j'ai travaillé sur ce projet avec d'autres profils (développeur, marketing). Nous nous sommes vraiment ancrés comme une cible potentiel, et réfléchissions sur ce que l’on aurait aimé avoir comme application. Le but était de fournir une expérience assez unique et de prendre un axe jeu et ludique, qui permettait de toucher beaucoup plus de jeunes et d'étudiants que les applications déjà existantes.",
    },
    {
      Id: "4",
      heading: "Blank",
      subHeading:
        "Projet d'installation d'un espace interactif pour la Gaîté Lyrique.",
      type: "Installation interactive",
      job: "UX/UI Designer & Lead Dev",
      date: "2017",
      videosId: "r_hYR53r61M",
      cover: "/static/images/portfolio/blank/cover.webp",
      pImg1: "/static/images/portfolio/blank/img-1.webp",
      pImg2: "/static/images/portfolio/blank/img-2.webp",
      pImg3: "/static/images/portfolio/blank/img-3.webp",
      vedio: "",
      text1:
        "[blank] est un espace interactif visant à retranscrire visuellement ce que pourrait être la mémoire informatique à travers les mouvements et les actions effectuées par les visiteurs. En utilisant la technologie des caméras Kinect, les mouvements des visiteurs sont enregistrés sous forme de mémoire informatique et retranscrit visuellement sur les murs de la salle.",
      text2:
        "L'utilisateur se déplacera dans quatre salles avec des univers graphiques différents mais aussi évolutifs, simulant chacun une représentation visuelle de la mémoire perçue par l’ordinateur. La forme graphique retranscrite ne cesse d'évoluer et se perfectionner de salle en salle, pour ressembler de plus en plus à une silhouette humaine.",
      text3:
        "Lorsque l'ordinateur commence à stocker un certain nombre de données en fonction de la progression du remplissage de l'écran, une barre de capacité mémorielle se remplit jusqu'à saturation atteinte. Lorsque cette limite est atteinte, elle stocke la dernière image laissée par les visiteurs en la figeant quelques secondes avant d'exploser en pixels pour laisser de nouveau un mur blanc et donc, une nouvelle mémoire à remplir. Une fois sorti de la 4ème salle, les visiteurs arrivent dans une cinquième et dernière salle, plongée dans le noir et éclairée par des écrans agencés comme une immense pellicule de cinema. Ils pourront alors admirer les dernières images enregistrés par les ordinateurs des différentes salles de l'expérience proposée par [blank].",
      quote:
        "C'était la première fois que je sortais du domaine pur des interfaces web et d'applications pour me consacrer sur une expérience complète où l'utilisateur était un acteur en lui-même. La partie la plus addictive à ce projet a été de le réaliser véritablement (via des logiciels comme Processing) et de l'exposer. De pouvoir itérer, développer en temps réel, voir le public intéragir avec, m'a fait découvrir et rendu amoureux du design et des espaces interactifs.",
    },
    {
      Id: "5",
      heading: "Free'in",
      subHeading:
        "L'app communautaire permettant aux utilisateurs de trouver les échantillons et produits gratuits à proximité.",
      type: "Application",
      job: "UX/UI Designer",
      date: "2019",
      videosId: "LUSa3yRTB9A",
      cover: "/static/images/portfolio/freein/cover.webp",
      pImg1: "/static/images/portfolio/freein/img-1.webp",
      pImg2: "/static/images/portfolio/freein/img-2.webp",
      pImg3: "/static/images/portfolio/freein/img-3.webp",
      vedio: "",
      text1:
        "Free'in est une application autour d'une carte communautaire permettant à ses utilisateurs de trouver autour d'eux les différents stands, ou campagnes promotionnelles de marques proposant des échantillons gratuits, tests… Les utilisateurs pourront profiter de ces offres s'ils sont à proximité. Le but de l'application était de permettre un usage ponctuel, mais aussi de satisfaire les utilisateurs toujours à l'affût des réductions, ou produits offerts…",
      text2:
        "L’enjeu principal était de proposer une application à la fois discrète et pouvant être utilisée de manière active. Les utilisateurs n’ont pas besoin d’avoir l’application ouverte, ils recevront une notification si un bon plan a été détecté dans un rayon personnalisable. L'utilisateur peut également filtrer les différents bons plans en fonction de ses intérêts (cosmétique, Alimentation,...). L'application étant communautaire, c'est à chacun de partager les offres trouvées et d’en informer les autres utilisateurs, qui eux-mêmes peuvent vérifier de la véracité de l'annonce. Tout ça afin de permettre un flux réel et constant d'informations.",
      text3:
        "L'application permet aussi à certaines marques de se mettre en avant et d'afficher leurs bons plans de manière sponsorisé, ce qui permettait d'avoir un côté gagnant-gagnant à la fois pour Free'in, les marques et les futurs consommateurs profitant des différents bon plans.",
      quote:
        "Ce projet m'a beaucoup appris sur les enjeux et les usages d'une application communautaire. Et aussi, de se questionner sur comment viser plusieurs types d’utilisateurs à la fois actif ou passif sans gêner l'expérience et la navigation de l'un ou de l'autre.",
    },
    {
      Id: "5",
      heading: "Viticool",
      subHeading:
        "Le service qui permet de rassembler viticulteurs et futurs saisonniers pendant les vendanges",
      type: "Site Internet",
      job: "UX/UI Designer",
      date: "2022",
      videosId: "r_hYR53r61M",
      cover: "/static/images/portfolio/viticool/cover.webp",
      pImg1: "/static/images/portfolio/viticool/img-1.webp",
      pImg2: "/static/images/portfolio/viticool/img-2.webp",
      pImg3: "/static/images/portfolio/viticool/img-3.webp",
      vedio: "",
      text1:
        "Viticool est une plateforme de type réseau social permettant de communiquer et de relier les viticulteurs et les saisonniers intéressés pour participer aux travaux viticoles. En effet, jusqu’à aujourd’hui, les méthodes de recrutement de personnel étaient souvent brouillonnes. Viticool propose donc une solution de plateforme dédiée afin de permettre une recherche plus simple et efficace.",
      text2:
        "En fonction du profil de l'utilisateur, il peut rejoindre un vignoble et discuter avec le viticulteur via la messagerie. Si le profil est accepté, il rejoint alors le groupe composé du viticulteur et des autres utilisateurs déjà embauchés.",
      text3:
        "Le viticulteur, de son côté, peut créer une page consacrée à son vignoble en y ajoutant les différentes informations nécessaires aux saisonniers : lieu, rémunération, restauration et logements,... De manière simple et visuelle. Il recevra également les demandes des différents saisonniers et pourra les administrer en quelques clics.",
      quote:
        "J'ai rejoint le projet qui était à son tout début en tant que freelance. Lorsque la start-up m'a donné le pitch du projet, j'étais très content de concevoir un outil dans ce secteur. Ayant des proches dans le milieu viticole, ce projet m'a de suite intéressé car je comprenais d'hors et déjà le besoin de mains chez certains viticulteurs.",
    },
  ];

  return (
    <div id="portfolio" className="tp-protfolio-area section-padding">
      <div className="container">
        <div className="col-12">
          <div className="section-title text-center" data-aos="fade-up">
            <span>Projets récents</span>
            <h2>Portfolio</h2>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <div className="tp-protfolio-item">
              <div className="row">
                {portfolio.map((port, prt) => (
                  <div
                    className="col-lg-4 col-md-6 col-sm-12 custom-grid"
                    data-aos="fade"
                    key={prt}
                  >
                    <div className="">
                      <div className="tp-protfolio-single">
                        <div className="tp-protfolio-img">
                          <Image
                            src={port.cover}
                            alt={port.heading}
                            width="435"
                            height="305"
                          />
                        </div>
                        <div className="tp-protfolio-text">
                          <h2>{port.heading}</h2>
                          <span>{port.subHeading}</span>
                          <button onClick={() => handleClickOpen(port)}>
                            <a>En savoir plus</a>
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
      <PortfolioSingle
        open={open}
        onClose={handleClose}
        title={state.heading}
        doc={state.doc}
        cover={state.cover}
        image1={state.pImg1}
        image2={state.pImg2}
        image3={state.pImg3}
        type={state.type}
        videosId={state.videosId}
        job={state.job}
        date={state.date}
        text1={state.text1}
        text2={state.text2}
        text3={state.text3}
        quote={state.quote}
      />

      <div className="white_svg svg_white">
        <svg x="0px" y="0px" viewBox="0 186.5 1920 113.5">
          <polygon points="0,300 655.167,210.5 1432.5,300 1920,198.5 1920,300 "></polygon>
        </svg>
      </div>
    </div>
  );
};

export default Portfolio;
