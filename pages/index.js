import React, {Fragment} from 'react';
import Hero from '../components/Hero/index';
import About from '../components/about/index';
import Service from '../components/Service/index';
import PricingPlan from '../components/Pricing/index';
import BlogSection from '../components/BlogSection/index';
import ContactSection from '../components/ContactSection/index';
import Footer from '../components/Footer/index';
import Testimonial from '../components/Testimonials/index';
import Portfolio from '../components/portfolio/index';
import Scrollbar from '../components/Scroolbar/index'
import Navbar from '../components/Navbar/index';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Element} from 'react-scroll'
import Head from "next/head"

const HomePage =() => {
    return(
        <>
        <Head>
            <title>Constantin Krieg | UX/UI Designer & Developper - Portfolio </title>
            <link rel="apple-touch-icon" sizes="180x180" href="static/favicon/apple-touch-icon.png"/>
            <link rel="icon" type="image/png" sizes="32x32" href="static/favicon/favicon-32x32.png"/>
            <link rel="icon" type="image/png" sizes="16x16" href="static/favicon/favicon-16x16.png"/>
            <link rel="manifest" href="static/favicon/site.webmanifest"/>
            <link rel="mask-icon" href="static/favicon/safari-pinned-tab.svg" color="#5bbad5"/>
            <meta name="msapplication-TileColor" content="#da532c"/>
            <meta name="theme-color" content="#ffffff"/>
            <meta name="google-site-verification" content="pZAzYb7zQvJFHSbyGoKdA88P-PcT9PJft11WCiyp4v8" />
            <script async defer src="https://tools.luckyorange.com/core/lo.js?site-id=d71f4e4c"></script>
            <meta name="description" content="Constantin Krieg - UX/UI Designer et Développeur Front-End. Portfolio. Passioné des interfaces, des interactions entre l'utilisateur et son environnement ainsi que des nouvelles technologies, je serais ravi de travailler à vos côtés - constantinkrieg@gmail.com"/>
        </Head>
        <Fragment>
            <div className="br-app">
                <Navbar />
                <Element name='home'>
                    <Hero/>
                </Element>
                <Element name='about'>
                    <About/>
                </Element>
                <Element name="service">
                    <Service/>
                </Element>
                <Element name="portfolio">
                    <Portfolio/>
                </Element>
                {/* <Testimonial/> */}
                {/* <PricingPlan/> */}
                <Element name="blog">
                    <BlogSection/>
                </Element>
                <Element name="contact">
                    <ContactSection/>
                </Element> 
                <Footer/>
                <Scrollbar/>
            </div>
        </Fragment>
        </>
    )
};
export default HomePage;


