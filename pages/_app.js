import { useEffect } from "react";
import AOS from "aos";
import "aos/dist/aos.css";

import "../styles/globals.css";
import "font-awesome/css/font-awesome.min.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/flaticon.css";
import "../styles/style.css";
import "../styles/Responsive.css";

function MyApp({ Component, pageProps }) {
  useEffect(() => {
    AOS.init({
      duration: 1000, // Animation duration in ms
      once: true, // Ensures animation happens only once while scrolling
      easing: "ease-in-out", // Smooth animation easing
    });
  }, []);

  return <Component {...pageProps} />;
}

export default MyApp;
